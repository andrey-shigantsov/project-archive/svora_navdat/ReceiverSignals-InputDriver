#include "ReceiverSignalsNavdatInterface.h"
#include "ReceiverSignalsInputDevice.h"

Q_DECLARE_METATYPE(QVector<qint8>)

ReceiverSignalsNavdatInterface::ReceiverSignalsNavdatInterface(QObject* parent) :
  ReceiverSignalsInterface(0, 0, parent)
{
  qRegisterMetaType<QVector<qint8> >("QVector<qint8>");
  qRegisterMetaType<QVector<quint8> >("QVector<quint8>");
}

QByteArray ReceiverSignalsNavdatInterface::convertScopeSpecParamsData(quint32 Count)
{
  QByteArray spec_data;
  QDataStream out(&spec_data, QIODevice::WriteOnly);
  out.setVersion(QDataStream::Qt_5_5);
  out << Count;
  return spec_data;
}

void ReceiverSignalsNavdatInterface::read(quint8 type, QDataStream& in)
{
  switch (type)
  {
  case requestConfig:{
    quint32 Fd = 0, Nfft = 0;
    in >> Fd >> Nfft;
    emit ConfigRequestData(Fd, Nfft);
    break;}

  case Scope:
    emit ScopeData(read_samples_iq(in));
    break;

  case SyncEstimations:
    emit SyncEstimationsData(read_samples(in), read_samples(in));
    break;

  case Spectrum:
    emit SpectrumData(read_samples_iq(in));
    break;

  case SignalDetected:{
    quint32 Idx = 0;
    in >> Idx;
    emit SignalDetectedData(Idx);
    break;}

  case Estimations:{
    float snr = 0;
    in >> snr;
    float freqOffset;
    in >> freqOffset;
    emit EstimationsData(snr,freqOffset);
    break;}

  case Equalizer:
    emit EqualizerData(read_samples(in));
    break;

  case PhaseOffset:
    emit PhaseOffsetData(read_samples(in));
    break;

  case Constellation:
    emit ConstellationData(read_samples_iq(in));
    break;

  case qamBuf:{
    quint8 xS;
    in >> xS;
    emit qamData(xS, read_vector<qint8>(in));
    break;}

  case encBuf:{
    quint8 xS;
    in >> xS;
    emit encData(xS, read_vector<quint8>(in));
    break;}

  case dataBuf:{
    quint8 xS;
    in >> xS;
    emit Data(xS, read_vector<quint8>(in));
    break;}

  default:
    break;
  }
}
